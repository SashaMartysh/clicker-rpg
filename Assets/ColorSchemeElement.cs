﻿using UnityEngine;
using UnityEngine.UI;

public class ColorSchemeElement : MonoBehaviour {

    void Start()
    {
        if (ColorScheme.Instance != null)
        {
            ColorScheme.Instance.onColorSchemeChange += ChangeColor;
            ChangeColor(ColorScheme.Instance.CurrentColor);
        }
    }

    void OnEnable()
    {
        if (ColorScheme.Instance != null)
        {
            ChangeColor (ColorScheme.Instance.CurrentColor);
        }
    }

    void ChangeColor (Color newColor)
    {
        Image image = GetComponent<Image>();
        if (image != null)
            image.color = newColor;
    }
}
