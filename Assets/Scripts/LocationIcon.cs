﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SmartLocalization;

public class LocationIcon : MonoBehaviour
{
    Button button;

    public Level level;

    [Header("Elements")]
    public TextMeshProUGUI nameLabel;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(LocationIsClicked);
    }

    void OnEnable()
    {
        RefreshInformation();
    }

    void LocationIsClicked()
    {
        Debug.Log("Location is clicked: " + level.name);
        LevelController.Instance.GoToLevel(level);
    }

    public void RefreshInformation()
    {
        nameLabel.text = LanguageManager.Instance.GetTextValue(level.name);
    }
}
