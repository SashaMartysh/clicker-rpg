﻿using UnityEngine;

public class VictoryScreen : MonoBehaviour {
    
    public void ExitToMainmenu()
    {
        Player.lastPlayedLocationId = 1;
        Player.Instance.SavePlayerData();
        GameManager.Instance.currentState = GameManager.State.MainMenu;
        for (int i = 0; i < MenuController.Instance.openedWindows.Count; i++)
            MenuController.Instance.openedWindows[i].gameObject.SetActive(false);
        MenuController.Instance.mainMenu.SetActive(true);
    }
}
