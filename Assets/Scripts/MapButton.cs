﻿using UnityEngine;

public class MapButton : MonoBehaviour
{
    public static MapButton Instance;
    public Animator animator;

    void Awake()
    {
        Instance = this;
    }

    public void ButtonIsClicked()
    {
        StopAnimation();
        MenuController.Instance.OpenCloseMap();
    }

    public void StartAnimation()
    {
        //ResetAnimatorTriggers();
        animator.SetTrigger("NewLocationOpened");
    }

    public void StopAnimation()
    {
       // ResetAnimatorTriggers();
        animator.SetTrigger("BackToNormalState");
    }

    //private void ResetAnimatorTriggers()
    //{
    //    animator.ResetTrigger("NewLocationOpened");
    //    animator.ResetTrigger("BackToNormalState");
    //}
}
