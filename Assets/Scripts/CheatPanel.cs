﻿using UnityEngine;

public class CheatPanel : MonoBehaviour {

    public void CheatMoney()
    {
        Player.gold *= 10f;
    }

    public void CheatExperience()
    {
        Player.experience *= 10f;
    }

    public void OpenNextLocation()
    {
        if (Player.maxLocationOpened < 17)
        {
            Player.maxLocationOpened++;
            MapButton.Instance.StartAnimation();
        }
    }
}
