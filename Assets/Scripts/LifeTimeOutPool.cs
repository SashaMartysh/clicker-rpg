﻿using UnityEngine;
using MarchingBytes; // Easy Pool

// для объектов, которые по истечении времени сами вовзращают себя в пул.
public class LifeTimeOutPool : MonoBehaviour
{
    public float lifeTime;

    // Use this for initialization
    void OnEnable()
    {
        Invoke("ReturnToPool", lifeTime);
    }

    private void ReturnToPool()
    {
        EasyObjectPool.instance.ReturnObjectToPool(gameObject);
    }
}
