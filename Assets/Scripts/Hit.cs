﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// некая структура. которая содержит информацию об ударе игрока по монстру (было ли попадание, был ли крит, каково повреждение?)
public struct Hit
{
    public bool isSuccess;
    public FNumber dmg;
    public bool isCritical;
    public Type type;


    public Hit(bool isSuccess, FNumber damage, bool isCritical, Type type)
    {
        this.isSuccess = isSuccess;
        this.dmg = damage;
        this.isCritical = isCritical;
        this.type = type;
    }

    public Hit(bool isSuccess, FNumber damage, bool isCritical) : this(isSuccess, damage, isCritical, Type.melee)
    {

    }

    public enum Type
    {
        melee = 1,
        lighting = 2,
        fire = 3,
        cold = 4
    }
}
