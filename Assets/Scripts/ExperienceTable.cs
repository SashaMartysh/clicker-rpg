﻿using UnityEngine;

[CreateAssetMenu(menuName = "Special Game Objects/Experience Table")]
public class ExperienceTable : ScriptableObject
{
    public string description;
    public FNumber[] levelRequirements;
}
