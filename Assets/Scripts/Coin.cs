﻿using UnityEngine;
using TMPro;
using MarchingBytes; // Easy Pool

// Падающая с врага монетка.
public class Coin : MonoBehaviour
{
    FNumber value;

    [Header("Elements and Links")]
    public TextMeshPro labelValue;
    //public GameObject coinImage;
    public AudioSource audioSource;
    public AudioClip[] coinSounds;
    public ParticleSystem particleSystem;
    public SprayedParticle particleBehaviour;

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("ParticleGround"))
        {
            Player.gold += value;
            audioSource.clip = coinSounds[Random.Range(0, coinSounds.Length)];
            audioSource.Play();
            labelValue.gameObject.SetActive(false);
            //coinImage.SetActive(false);
            particleSystem.Play();
            Invoke("ReturnToPool", 1f);
        }
    }

    private void ReturnToPool()
    {
        EasyObjectPool.instance.ReturnObjectToPool (gameObject);
    }

    public void RepresentCoin (FNumber newValue)
    {
        value = newValue;
        labelValue.text = value.ToString() + "<sprite=0>";
        labelValue.gameObject.SetActive(true);
        transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(-10f, 10f));
        //coinImage.SetActive(true);
        particleBehaviour.ImitateParticle();
    }

}
