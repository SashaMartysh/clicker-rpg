﻿using UnityEngine;

public class Map : MonoBehaviour
{
    public LocationIcon [] locationIcons;

    void OnEnable()
    {
        for (int i = 0; i < locationIcons.Length; i++)
        {
            if (locationIcons[i].level.id > Player.maxLocationOpened)
                locationIcons[i].gameObject.SetActive(false);
            else locationIcons[i].gameObject.SetActive(true);
        }
    }
}
