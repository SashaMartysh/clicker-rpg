﻿using System.Collections.Generic;
using UnityEngine;

    public class ProportionalWheelSelection
    {
        public static System.Random rnd = new System.Random();

        // Static method for using from anywhere. You can make its overload for accepting not only List, but arrays also: 
        // public static Item SelectItem (Item[] items)...
        public static GameObject SelectMonster(List<MonsterWithChance> pool)
        {
            // Calculate the summa of all portions.
            int poolSize = 0;
            for (int i = 0; i < pool.Count; i++)
            {
                poolSize += pool[i].chance;
            }

            // Get a random integer from 0 to PoolSize.
            int randomNumber = rnd.Next(0, poolSize) + 1;

            // Detect the item, which corresponds to current random number.
            int accumulatedProbability = 0;
            for (int i = 0; i < pool.Count; i++)
            {
                accumulatedProbability += pool[i].chance;
                if (randomNumber <= accumulatedProbability)
                    return pool[i].monster;
            }
            return null;    // this code will never come while you use this programm right :)
        }

    public static GameObject SelectMonster(MonsterWithChance [] pool)
    {
        // Calculate the summa of all portions.
        int poolSize = 0;
        for (int i = 0; i < pool.Length; i++)
        {
            poolSize += pool[i].chance;
        }

        // Get a random integer from 0 to PoolSize.
        int randomNumber = rnd.Next(0, poolSize) + 1;

        // Detect the item, which corresponds to current random number.
        int accumulatedProbability = 0;
        for (int i = 0; i < pool.Length; i++)
        {
            accumulatedProbability += pool[i].chance;
            if (randomNumber <= accumulatedProbability)
                return pool[i].monster;
        }
        return null;    // this code will never come while you use this programm right :)
    }
}


