﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class Pool<T> where T : MonoBehaviour
{
    public Action<T> onGameObjectCreated;

    private GameObject prefab;
    private Transform parent;

    private List<T> pool;
    private int lastActiveIndex;

    public Pool(GameObject prefab, Transform parent, int startSize = 10, Action<T> onGameObjectCreated = null)
    {
        this.prefab = prefab;
        this.parent = parent;
        this.onGameObjectCreated += onGameObjectCreated;

        pool = new List<T>();
        lastActiveIndex = -1;

        AddElem(startSize);

    }

    private void CheckSize(int size)
    {
        if (Length <= size)
            AddElem(size - Length + 1, Length);
    }

    private void AddElem(int count, int fromAdd = 0)
    {
        if (fromAdd < 0)
            fromAdd = 0;

        for (int i = fromAdd; i < count + fromAdd; i++)
        {
            T newGO = GameObject.Instantiate(prefab, parent, false).GetComponent<T>();
            pool.Add(newGO);
            onGameObjectCreated.InvokeSafe(newGO);
            pool[i].gameObject.SetActive(false);
        }
    }

    public List<T> GetArray()
    {
        return pool;
    }

    public int Length { get { return pool == null ? 0 : pool.Count; } }

    public T this[int i]
    {
        get
        {
            ActiveByIndex(i);
            return pool[i];
        }
    }

    public void ActiveByIndex(int i)
    {
        CheckSize(i + 1);

        if (!pool[i].gameObject.activeSelf)
            pool[i].gameObject.SetActive(true);

        lastActiveIndex = i;
    }

    public void DeactNonActive()
    {
        for (int i = lastActiveIndex + 1; i < pool.Count; i++)
        {
            if (pool[i].gameObject.activeSelf)
                pool[i].gameObject.SetActive(false);
        }
    }

    public void DeactiveAll()
    {
        lastActiveIndex = -1;
        DeactNonActive();
    }

    public int GetNumberOfActive()
    {
        return lastActiveIndex + 1;
    }
}
