﻿using System;
using System.Collections;
using System.Collections.Generic;
// using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

public static class ExtensionMethods
{
    public static void LookAt2D(this Transform transform, Transform point)
    {
        Vector2 dirV = point.position - transform.position;

		float rot_z = Mathf.Atan2(dirV.y, dirV.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z);
    }

    private static Toggle.ToggleEvent emptyEvent = new Toggle.ToggleEvent();

    public static void InvokeNextFrame(this MonoBehaviour monoBehaviour, Action method)
    {
        monoBehaviour.StartCoroutine(InvokeNextFrameCoroutine(method));
    }

    private static IEnumerator InvokeNextFrameCoroutine(Action method)
    {
        yield return null;
        method();
    }

    public static void FocusOn(this ScrollRect scroll, RectTransform objectToFocus)
    {
        HorizontalLayoutGroup layoutGroup = scroll.content.GetComponent<HorizontalLayoutGroup>();
        Vector2 newPosition = scroll.content.anchoredPosition;
        newPosition.x = -(objectToFocus.anchoredPosition.x - layoutGroup.padding.left);
        scroll.content.anchoredPosition = newPosition;
    }

    //делает перевод строки
    //и переводит название в верхний регистр
    public static string FormatSongTheme(this string themeName)
    {
        string[] words = themeName.Split(' ');
		MutableString mString = MutableString.Start;

		for(int i = 0; i < words.Length; i++)
		{
			if(words[i].Length > 3)
			{
				mString.Add(words[i]).Add("\n");
			}
			else
			{
				mString.Add(words[i]).Add(" ");
			}
		}

		return mString.ToString().ToUpper();
    }

    public static string FormatPercent(this float percent)
    {
        return string.Format("{0:0.#}%", percent);
    }

    public static void InvokeSafe(this Action self)
    {
        if (self != null) self.Invoke();
    }

    public static void InvokeSafe<T>(this Action<T> self, T data)
    {
        if(self != null) self(data);
    }

    public static ValType GetSafe<KeyType, ValType>(this Dictionary<KeyType, ValType> self, KeyType key)
    {
        if (self.ContainsKey(key))
            return self[key];

        return default(ValType);
    }

    public static ValType GetSafe<ValType>(this List<ValType> self, int index)
    {
        if (index < 0 || index >= self.Count)
            return default(ValType);

        return self[index];
    }

    public static void AddSafe<KeyType, ValType>(this Dictionary<KeyType, ValType> self, KeyType key, ValType val)
    {
        if (self.ContainsKey(key))
            self[key] = val;
        else
            self.Add(key, val);
    }

    public static void AddSafe<ValType>(this List<ValType> self, ValType val, int index)
    {
        if(index >= self.Count)
            self.Add(val);
        else
            self[index] = val;
    }

    public static Coroutine RestartCoroutine(this MonoBehaviour self, IEnumerator routine)
    {
        self.StopAllCoroutines();
        return self.StartCoroutine(routine);
    }

    public static void RemoveAll<T>(this LinkedList<T> list, Predicate<T> match)
    {
        var node = list.First;
        while (node != null)
        {
            var next = node.Next;
            if (match(node.Value))
            {
                list.Remove(node);
            }
            node = next;
        }
    }

    public static Color fYellow (this Color color)
    {
        return new Color(0.992156f, 0.992156f, 0.780392f);
    }

    // public static bool IsNull(this JSONNode self)
    // {
    //     return self == null || self.Count == 0;
    // }

}