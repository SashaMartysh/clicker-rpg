﻿using UnityEngine;

public static class MyColors {

    public static Color LightYellow()
    {
        return new Color(0.992156f, 0.992156f, 0.780392f);
    }

    public static Color Gold()
    {
        return new Color(1f, 0.843137f, 0f, 0.780392f);
    }

    public static Color ColdElement()
    {
        return new Color(0.494117f, 0.7294f, 0.8117f);
    }

    public static Color FireElement()
    {
        return new Color(0.9843f, 0.5098f, 0f);
    }

    public static Color Grey()
    {
        return new Color(0.8156f, 0.8156f, 0.7725f);
    }
}
