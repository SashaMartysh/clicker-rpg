﻿using System;
using System.Text;
using UnityEngine;

// "Fantastic Number" is used for operate such numbers: 0.05, 500, 2K, 3.5M, 500B etc...
[Serializable]
public struct FNumber
{
    public float value;
    public Rank rank;

    public FNumber(float value) : this(value, Rank.units)
    { }

    public FNumber(int value) : this(value, Rank.units)
    { }

    public FNumber(long value) : this(value, Rank.units)
    { }

    public FNumber(float value, Rank rank)
    {
        this.value = value;
        this.rank = rank;
        Refresh();
    }

    // Обновляет число, чтобы числовое значение было не больше 999 и не менее 1.
    private void Refresh()
    {
        while (Math.Abs(this.value) > 1000f && (int)this.rank < 8)
        {
            this.value /= 1000f;
            this.rank++;
        }
        while (Math.Abs(this.value) < 1.0f && this.rank > 0)
        {
            this.value *= 1000f;
            this.rank--;
        }
    }

    // Показывает лишь три значащие знака в числе.
    public override string ToString()
    {
        Refresh();
        StringBuilder strBuilder = new StringBuilder();
        if (rank == Rank.units)
        {
            // Если число до 1000 единиц, то дробные части вообще не показываем.
            strBuilder.Append(value.ToString("###."));
        }
        else
        {
            if (Math.Abs(value) >= 100f)
                strBuilder.Append(value.ToString("###."));
            else if (Math.Abs(value) >= 10f)
                strBuilder.Append(value.ToString("##.#"));
            else
                strBuilder.Append(value.ToString("0.##"));
        }
        if (rank > 0)
            strBuilder.Append(rank.ToString().Substring(0,1));
        return strBuilder.ToString();
    }

    public float ToFloat()
    {
        FNumber fn = new FNumber (value, rank);
        while (fn.rank > 0)
        {
            fn.value *= 1000f;
            fn.rank--;
        }
        return fn.value;
    }

    public int ToInt()
    {
        FNumber fn = new FNumber(value, rank);
        while (fn.rank > 0)
        {
            fn.value *= 1000f;
            fn.rank--;
        }
        return (int)fn.value;
    }

    public long ToLong()
    {
        FNumber fn = new FNumber(value, rank);
        while (fn.rank > 0)
        {
            fn.value *= 1000f;
            fn.rank--;
        }
        return (long)fn.value;
    }

    // Операция сложения. 
    public static FNumber operator +(FNumber fn1, FNumber fn2)
    {
        // Привожу числа к одному рангу.
        if (fn1.rank > fn2.rank)
        {
            while (fn2.rank < fn1.rank)
            {
                fn2.value /= 1000f;
                fn2.rank++;
            }
        }
        else if (fn1.rank < fn2.rank)
        {
            while (fn1.rank < fn2.rank)
            {
                fn1.value /= 1000f;
                fn1.rank++;
            }
        }

        // Добавляю и возвращаю.
        return new FNumber(fn1.value + fn2.value, fn1.rank);
    }
    public static FNumber operator +(FNumber fNumber, float f)
    {
        return (fNumber + new FNumber(f, Rank.units));
    }
    public static FNumber operator +(FNumber fNumber, int i)
    {
        return (fNumber + new FNumber(i, Rank.units));
    }
    public static FNumber operator +(float f, FNumber fNumber)
    {
        return (fNumber + new FNumber(f, Rank.units));
    }
    public static FNumber operator +(int i, FNumber fNumber)
    {
        return (fNumber + new FNumber(i, Rank.units));
    }

    // Операция вычитания. 
    public static FNumber operator -(FNumber fn1, FNumber fn2)
    {
        // Привожу числа к одному рангу.
        if (fn1.rank > fn2.rank)
        {
            while (fn2.rank < fn1.rank)
            {
                fn2.value /= 1000f;
                fn2.rank++;
            }
        }
        else if (fn1.rank < fn2.rank)
        {
            while (fn1.rank < fn2.rank)
            {
                fn1.value /= 1000f;
                fn1.rank++;
            }
        }

        // Делаю вычитание.
        return new FNumber(fn1.value - fn2.value, fn1.rank);
    }
    public static FNumber operator -(FNumber fn, float f)
    {
        return (fn - new FNumber(f, Rank.units));
    }
    public static FNumber operator -(FNumber fn, int i)
    {
        return (fn - new FNumber(i, Rank.units));
    }
    public static FNumber operator -(float f, FNumber fn)
    {
        return (new FNumber(f, Rank.units) - fn);
    }
    public static FNumber operator -(int i, FNumber fn)
    {
        return (new FNumber(i, Rank.units) - fn);
    }
    public static FNumber operator -(FNumber fNumber)
    {
        // Возвращаем с противоположным знаком.
        return new FNumber(-1 * fNumber.value, fNumber.rank);
    }

    // Оператор умножения. // Multiplication operator.
    public static FNumber operator *(FNumber fn, float f)
    {
        return new FNumber(fn.value * f, fn.rank);
    }
    public static FNumber operator *(FNumber fn, int i)
    {
        return new FNumber(fn.value * i, fn.rank);
    }
    public static FNumber operator *(float f, FNumber fn)
    {
        return new FNumber(fn.value * f, fn.rank);
    }
    public static FNumber operator *(int i, FNumber fn)
    {
        return new FNumber(fn.value * i, fn.rank);
    }

    // Оператор деления. // Division operator.
    public static FNumber operator /(FNumber fn, float f)
    {
        return new FNumber(fn.value / f, fn.rank);
    }
    public static FNumber operator /(FNumber fn, int i)
    {
        return new FNumber(fn.value / i, fn.rank);
    }
    public static float operator /(float f, FNumber fn)
    {
        // Понижаю ранг до обычных чисел.
        while (fn.rank > 0)
        {
            fn.value *= 1000f;
            fn.rank--;
        }

        return f / fn.value;
    }
    public static float operator /(int i, FNumber fn)
    {
        // Понижаю ранг до обычных чисел.
        while (fn.rank > 0)
        {
            fn.value *= 1000f;
            fn.rank--;
        }

        return i / fn.value;
    }
    public static float operator /(FNumber fn1, FNumber fn2)
    {
        // Привожу числа к одному рангу.
        if (fn1.rank > fn2.rank)
        {
            while (fn2.rank < fn1.rank)
            {
                fn2.value /= 1000f;
                fn2.rank++;
            }
        }
        else if (fn1.rank < fn2.rank)
        {
            while (fn1.rank < fn2.rank)
            {
                fn1.value /= 1000f;
                fn1.rank++;
            }
        }

        return fn1.value / fn2.value;
    }

    public static bool operator ==(FNumber fNumber1, FNumber fNumber2)
    {
        if (fNumber1.rank == fNumber2.rank && fNumber1.value == fNumber2.value)
            return true;
        else return false;
    }

    public static bool operator !=(FNumber fNumber1, FNumber fNumber2)
    {
        if (fNumber1.rank == fNumber2.rank && fNumber1.value == fNumber2.value)
            return false;
        else return true;
    }

    // Оператор сравнения
    public static bool operator <(FNumber fNumber1, FNumber fNumber2)
    {
        //// Сравниваем по знаку.
        //if (fNumber1.value < 0f && fNumber2.value > 0f)
        //    return true;
        //if (fNumber1.value > 0f && fNumber2.value < 0f)
        //    return false;

        //// Если знаки у них одинаковые, то пытаемся сравнить по рангу.
        //if (fNumber1.value > 0f)
        //{
        //    // В плюсовом диапазон высший ранг означает большее число.
        //    if (fNumber1.rank < fNumber2.rank)
        //        return true;
        //    if (fNumber1.rank > fNumber2.rank)
        //        return false;
        //}
        //else
        //{
        //    // В отрицательном диапазон высший ранг означает меньшее число.
        //    if (fNumber1.rank > fNumber2.rank)
        //        return true;
        //    if (fNumber1.rank < fNumber2.rank)
        //        return false;
        //}

        //// Если ранги равны, то сравниваем по значению.
        //if (fNumber1.value < fNumber2.value)
        //    return true;
        //if (fNumber1.value > fNumber2.value)
        //    return false;
        if (fNumber1.ToFloat() < fNumber2.ToFloat())
            return true;
        return false;
    }
    public static bool operator <(FNumber fNumber1, float f)
    {
        return (fNumber1.ToFloat() < f);
    }

    // Оператор сравнения
    public static bool operator >(FNumber fNumber1, FNumber fNumber2)
    {
        //// Сравниваем по знаку.
        //if (fNumber1.value > 0f && fNumber2.value < 0f)
        //    return true;
        //if (fNumber1.value < 0f && fNumber2.value > 0f)
        //    return false;

        //// Если знаки у них одинаковые, то пытаемся сравнить по рангу.
        //if (fNumber1.value > 0f)
        //{
        //    // В плюсовом диапазон высший ранг означает большее число.
        //    if (fNumber1.rank > fNumber2.rank)
        //        return true;
        //    if (fNumber1.rank < fNumber2.rank)
        //        return false;
        //}
        //else
        //{
        //    // В отрицательном диапазон высший ранг означает меньшее число.
        //    if (fNumber1.rank < fNumber2.rank)
        //        return true;
        //    if (fNumber1.rank > fNumber2.rank)
        //        return false;
        //}

        //// Если ранги равны, то сравниваем по значению.
        //if (fNumber1.value > fNumber2.value)
        //    return true;
        //if (fNumber1.value < fNumber2.value)
        //    return false;
        if (fNumber1.ToFloat() > fNumber2.ToFloat())
            return true;
        return false;
    }
    public static bool operator > (FNumber fNumber1, float f)
    {
        return (fNumber1.ToFloat() > f);
    }

    // Оператор сравнения
    public static bool operator <=(FNumber fNumber1, FNumber fNumber2)
    {
        //// Сравниваем по знаку.
        //if (fNumber1.value < 0f && fNumber2.value > 0f)
        //    return true;
        //if (fNumber1.value > 0f && fNumber2.value < 0f)
        //    return false;

        //// Если знаки у них одинаковые, то пытаемся сравнить по рангу.
        //if (fNumber1.value > 0f)
        //{
        //    // В плюсовом диапазон высший ранг означает большее число.
        //    if (fNumber1.rank < fNumber2.rank)
        //        return true;
        //    if (fNumber1.rank > fNumber2.rank)
        //        return false;
        //}
        //else
        //{
        //    // В отрицательном диапазон высший ранг означает меньшее число.
        //    if (fNumber1.rank > fNumber2.rank)
        //        return true;
        //    if (fNumber1.rank < fNumber2.rank)
        //        return false;
        //}

        //// Если ранги равны, то сравниваем по значению.
        //if (fNumber1.value <= fNumber2.value)
        //    return true;
        //if (fNumber1.value > fNumber2.value)
        //    return false;
        if (fNumber1.ToFloat() <= fNumber2.ToFloat())
            return true;
        return false;
    }

    public static bool operator <= (FNumber fn, float f)
    {
        if (fn.ToFloat() <= f)
            return true;
        else return false;
    }



    // Оператор сравнения
    public static bool operator >=(FNumber fNumber1, FNumber fNumber2)
    {
        //// Сравниваем по знаку.
        //if (fNumber1.value > 0f && fNumber2.value < 0f)
        //    return true;
        //if (fNumber1.value < 0f && fNumber2.value > 0f)
        //    return false;
        //// Если знаки у них одинаковые, то пытаемся сравнить по рангу.
        //if (fNumber1.value > 0f)
        //{
        //    // В плюсовом диапазон высший ранг означает большее число.
        //    if (fNumber1.rank > fNumber2.rank)
        //    {
        //        return true;
        //    }
        //    if (fNumber1.rank < fNumber2.rank)
        //        return false;
        //}
        //else if (fNumber1.value < 0f)
        //{
        //    // В отрицательном диапазон высший ранг означает меньшее число.
        //    if (fNumber1.rank < fNumber2.rank)
        //        return true;
        //    if (fNumber1.rank > fNumber2.rank)
        //        return false;
        //}
        //// Если ранги равны, то сравниваем по значению.
        //if (fNumber1.value >= fNumber2.value)
        //    return true;
        //if (fNumber1.value < fNumber2.value)
        //    return false;
        if (fNumber1.ToFloat() >= fNumber2.ToFloat())
            return true;
        return false;
    }
    public static bool operator >=(FNumber fn, float f)
    {
        if (fn.ToFloat() >= f)
            return true;
        else return false;
    }

    // Names of large numbers, how they are used in such countries: US, English Canadian, Australian, and modern British.
    public enum Rank
    {
        units = 0,
        K = 1, // thousands (kilos)
        M = 2, // millions      10^6
        B = 3, // billions      10^9
        T = 4, // trillions     10^12
        quadrillions = 5, // quadrillions  10^15
        Quintillion = 6, // quintillion   10^18
        sextillions = 7, // sextillions   10^21
        Septillions = 8  // septillion    10^24
    }
}