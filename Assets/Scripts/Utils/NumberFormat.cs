﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

public static class NumberFormat {

    // NOT FINISHED METHOD. NEED TO REMAKE.
    //public static string WithSuffix(this ulong number)
    //{
    //    if (number < 1000)
    //        return number.ToString();
    //    else if (number < 10000)
    //        return (number/1000f).ToString("#.##k");
    //    else if (number < 100000)
    //        return (number/10000f).ToString("##.#k");
    //    else if (number < 1000000)
    //        return (number/100000f).ToString("##.#k");
    //    else return "0";
    //}

    public static string WithSuffix(this ulong number)
    {
        if (number < 1000)
            return number.ToString();

        string workString = number.ToString("N", new CultureInfo("en-Us"));
        string subString = workString.Substring(0, 3);
        if (subString.Contains(","))
            subString = workString.Substring(0, 4);

        if (number < 1000000)
            return subString + "k";
        else if (number < 1000000000)
            return subString + "m";
        else if (number < 1000000000000)
            return subString + "B";
        else if (number < 1000000000000000)
            return subString + "T";
        else return subString + "Q";
    }

    public static string WithPrefix(this int _number)
    {
        return ((float)_number).WithPrefix();
    }

    public static string WithPrefix(this ulong _number)
    {
        return ((float)_number).WithPrefix();
    }

    public static string WithPrefix(this float _number)
    {
        float absNumber = Mathf.Abs(_number);

        if (absNumber < 1000)
        {
            _number = (float)System.Math.Round(_number);
            return _number.ToString();
        }
        else if (absNumber < 1000000)
        {
            return retPref(_number, "k", 1000);
        }
        else if (absNumber < 1000000000)
        {
            return retPref(_number, "m", 1000000);
        }
        else
        {
            return retPref(_number, "b", 1000000000);
        }
    }

    private static string retPref(float _number, string suff, ulong div)
    {
        float withRound = _number / div;
        return (System.Math.Round(withRound, 2)).ToString(".00") + suff;
    }
}
