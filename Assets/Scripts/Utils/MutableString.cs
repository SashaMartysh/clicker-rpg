﻿using System.Text;

public class MutableString
{

    static MutableString _ms;
    StringBuilder sb = new StringBuilder();

    public static MutableString Start
    {
        get
        {
            if (_ms == null)
            {
                _ms = new MutableString();
            }

            return _ms;
        }
    }

    #region Typed Methods

    public MutableString Add(string s)
    {
        sb.Append(s);
        return this;
    }

    public MutableString Add(int o)
    {
        sb.Append(o.ToString());
        return this;
    }

    public MutableString Add(float o)
    {
        sb.Append(o.ToString());
        return this;
    }

    public MutableString Add(double o)
    {
        sb.Append(o.ToString());
        return this;
    }

    public MutableString Add(object o)
    {
        sb.Append(o.ToString());
        return this;
    }

    #endregion

    /// <summary>
    /// Возвращает результируюзщую строку
    /// </summary>
    public override string ToString()
    {
        string s = sb.ToString();

        sb.Length = 0;

        return s;
    }
}