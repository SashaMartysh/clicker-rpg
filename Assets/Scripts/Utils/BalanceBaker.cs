﻿using StaticData;
using UnityEngine;

// Этот класс предназначен для генерации данных (по формулам или вручную) и запекания их в скриптабл-объектах.
public class BalanceBaker : MonoBehaviour
{
    [Header("Files for baking")]
    public ExperienceTable experienceTable;
    public Weapon woodenWeapon;
    public Weapon bronzeWeapon;
    public Weapon silverWeapon;
    public Weapon goldWeapon;
    public Spell lightingStrikeSpell;
    public Spell fireballSpell;
    public Spell iceShardsSpell;

    [Header("Options")]
    public bool needLogs = false;

    [Header("Resource Files")]
    public WoodenWeaponUpgraidesStatic oldWoodenUpgraidesData;

    [ContextMenu("Bake Balance Data")] // bake it in the Editor now!
    public void BakeAllBalances()
    {
        MakeExperienceTable();
        MakeWoodenWeaponUpgraides();
        MakeBronzeWeaponUpgraides();
        MakeSilverWeaponUpgraides();
        MakeGoldWeaponUpgraides();
        MakeLightingStrikeUpgraides2();
        MakeFireballUpgraides();
        MakeIceShardsUpgraides();
        Debug.Log("All Balance Data was baked successfully.");
    }



    // Создание  таблицы опыта. 
    public void MakeExperienceTable()
    {
        FNumber[] newExpTable = new FNumber[200];
        newExpTable[0] = new FNumber(0);
        newExpTable[1] = new FNumber(100);
        newExpTable[2] = new FNumber(250);
        for (int i = 3; i < newExpTable.Length; i++)
        {
            newExpTable[i] = newExpTable[i - 1] * (1.5f - (i/(newExpTable.Length * 2.15f)));
            if (needLogs)
                Debug.Log("Experience level " + i + " require: " + newExpTable[i].ToString());
        }
        experienceTable.levelRequirements = newExpTable; // bake
    }

    // импорт улучшений для деревянного оружия, сделанных раньше в Екселе. Облом перебивать формулами. ))
    public void MakeWoodenWeaponUpgraides()
    {
        WeaponUpgraide[] newWoodenUps = new WeaponUpgraide[oldWoodenUpgraidesData.woodenWeaponUpgraidesList.Length];
        for (int i = 0; i < oldWoodenUpgraidesData.woodenWeaponUpgraidesList.Length; i++)
        {
            newWoodenUps[i].damage = new FNumber(oldWoodenUpgraidesData.woodenWeaponUpgraidesList[i].dmg);
            newWoodenUps[i].cost = new FNumber(oldWoodenUpgraidesData.woodenWeaponUpgraidesList[i].cost);
            if (needLogs)
                Debug.Log("Setting Wooden Weapon Ups: " + i + " cost: " + newWoodenUps[i].cost + " dmg: " + newWoodenUps[i].damage);
        }

        woodenWeapon.upgraides = newWoodenUps; // bake.
    }

    // Настройка улучшений через формулы для оружия второго ранга.
    public void MakeBronzeWeaponUpgraides()
    {
        WeaponUpgraide [] newBronzeUps = new WeaponUpgraide[50];
        newBronzeUps[0].damage = new FNumber(2000);
        newBronzeUps[0].cost = new FNumber(10000);
        for (int i = 1; i < newBronzeUps.Length; i++)
        {
            newBronzeUps[i].damage = newBronzeUps[i - 1].damage * 1.1f;
            newBronzeUps[i].cost = newBronzeUps[i - 1].cost * 1.1f;
            if (needLogs)
                Debug.Log("Setting Bronze Ups: " + i + " cost: " + newBronzeUps[i].cost + " dmg: " + newBronzeUps[i].damage);
        }

        bronzeWeapon.upgraides = newBronzeUps; // bake.
    }

    // Настройка улучшений через формулы для оружия третьего ранга.
    public void MakeSilverWeaponUpgraides()
    {
        WeaponUpgraide[] newSilverUps = new WeaponUpgraide[50];
        newSilverUps[0].damage = new FNumber(1, FNumber.Rank.M);
        newSilverUps[0].cost = new FNumber(10, FNumber.Rank.M);
        for (int i = 1; i < newSilverUps.Length; i++)
        {
            newSilverUps[i].damage = newSilverUps[i - 1].damage * 1.2f;
            newSilverUps[i].cost = newSilverUps[i - 1].cost * 1.2f;
            if (needLogs)
                Debug.Log("Setting Silver Ups: " + i + " cost: " + newSilverUps[i].cost + " dmg: " + newSilverUps[i].damage);
        }

        silverWeapon.upgraides = newSilverUps; // bake.
    }

    // Настройка улучшений через формулы для оружия четвертого ранга.
    public void MakeGoldWeaponUpgraides()
    {
        WeaponUpgraide[] newGoldUps = new WeaponUpgraide[50];
        newGoldUps[0].damage = new FNumber(10, FNumber.Rank.B);
        newGoldUps[0].cost = new FNumber(100, FNumber.Rank.B);
        for (int i = 1; i < newGoldUps.Length; i++)
        {
            newGoldUps[i].damage = newGoldUps[i - 1].damage * 1.3f;
            newGoldUps[i].cost = newGoldUps[i - 1].cost * (1.3f + i / 1000f);
            if (needLogs)
                Debug.Log("Setting Gold Ups: " + i + " cost: " + newGoldUps[i].cost + " dmg: " + newGoldUps[i].damage);
        }

        goldWeapon.upgraides = newGoldUps; // bake.
    }

    // Настройка улучшений через формулы для заклинания Удар Молнии.
    public void MakeLightingStrikeUpgraides2()
    {
        SpellUpgraide [] newUpgraides = new SpellUpgraide[100];
        newUpgraides[0].damage = new FNumber(500f);
        newUpgraides[0].cost = new FNumber(500f);
        newUpgraides[0].manaCost = new FNumber(4f);
        for (int i = 1; i < newUpgraides.Length; i++)
        {
            newUpgraides[i].damage = newUpgraides[i - 1].damage * (1.56f - (i / (newUpgraides.Length * 2.5f)));
            newUpgraides[i].cost = newUpgraides[i - 1].cost * (1.56f - (i / (newUpgraides.Length * 2.35f)));
            newUpgraides[i].manaCost = newUpgraides[i - 1].manaCost + 1.5f;
            if (needLogs)
                Debug.Log("Setting Lighting Strike Ups: " + i + " Cost: " + newUpgraides[i].cost + " Damage: " + newUpgraides[i].damage + " Manacost: " + newUpgraides[i].manaCost);
        }

        lightingStrikeSpell.upgraides = newUpgraides; // bake
    }

    // Настройка улучшений через формулы для заклинания Fireball.
    public void MakeFireballUpgraides()
    {
        SpellUpgraide[] newUpgraides = new SpellUpgraide[100];
        newUpgraides[0].damage = new FNumber(250f);
        newUpgraides[0].cost = new FNumber(300f);
        newUpgraides[0].manaCost = new FNumber(2f);
        for (int i = 1; i < newUpgraides.Length; i++)
        {
            newUpgraides[i].damage = newUpgraides[i - 1].damage * (1.56f - (i / (newUpgraides.Length * 2.5f)));
            newUpgraides[i].cost = newUpgraides[i - 1].cost * (1.56f - (i / (newUpgraides.Length * 2.35f)));
            newUpgraides[i].manaCost = newUpgraides[i - 1].manaCost + 0.75f;
            if (needLogs)
                Debug.Log("Setting Fireball Ups: " + i + " Cost: " + newUpgraides[i].cost + " Damage: " + newUpgraides[i].damage + " Manacost: " + newUpgraides[i].manaCost);
        }

        fireballSpell.upgraides = newUpgraides; // bake
    }

    // Настройка улучшений через формулы для заклинания Ice Shards.
    public void MakeIceShardsUpgraides()
    {
        SpellUpgraide[] newUpgraides = new SpellUpgraide[100];
        newUpgraides[0].damage = new FNumber(1f, FNumber.Rank.K);
        newUpgraides[0].cost = new FNumber(1, FNumber.Rank.K);
        newUpgraides[0].manaCost = new FNumber(8f);
        for (int i = 1; i < newUpgraides.Length; i++)
        {
            newUpgraides[i].damage = newUpgraides[i - 1].damage * (1.56f - (i/ (newUpgraides.Length*2.5f)));
            newUpgraides[i].cost = newUpgraides[i - 1].cost * (1.56f - (i / (newUpgraides.Length * 2.35f)));
            newUpgraides[i].manaCost = newUpgraides[i - 1].manaCost + 2.75f;
            if (needLogs)
                Debug.Log("Setting Ice Shards Ups: " + i + " Cost: " + newUpgraides[i].cost + " Damage: " + newUpgraides[i].damage + " Manacost: " + newUpgraides[i].manaCost);
        }

        iceShardsSpell.upgraides = newUpgraides; // bake
    }
}
