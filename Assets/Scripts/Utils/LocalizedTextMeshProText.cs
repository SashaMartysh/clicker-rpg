﻿namespace SmartLocalization.Editor
{
    using UnityEngine;
    using TMPro;

    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedTextMeshProText : MonoBehaviour
    {
        public string localizedKey = "INSERT_KEY_HERE";
        TextMeshProUGUI textObject;

        [Header("Additional Options")]
        public bool toUpperCase = false;
        public bool toLowerCase = false;
        public bool withColon = false;

        void Start()
        {
            textObject = this.GetComponent<TextMeshProUGUI>();

            //Subscribe to the change language event
            LanguageManager languageManager = LanguageManager.Instance;
            languageManager.OnChangeLanguage += OnChangeLanguage;

            //Run the method one first time
            OnChangeLanguage(languageManager);
        }

        void OnDestroy()
        {
            if (LanguageManager.HasInstance)
            {
                LanguageManager.Instance.OnChangeLanguage -= OnChangeLanguage;
            }
        }

        void OnChangeLanguage(LanguageManager languageManager)
        {
            string text = LanguageManager.Instance.GetTextValue(localizedKey);
            if (toUpperCase)
                text.ToUpper();
            if (toLowerCase)
                text.ToLower();
            if (withColon)
                text = text + ":";
            textObject.text = text;
        }
    }
}