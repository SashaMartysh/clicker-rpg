﻿using UnityEngine;

// Массив FNumbers
[CreateAssetMenu(menuName = "Balance/BalanceData_1FNumbers")]
public class BalanceData : ScriptableObject
{
    public string description;
    public FNumber[] data;
}

