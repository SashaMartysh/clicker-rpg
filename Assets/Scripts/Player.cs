﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

// Тестирую, насколько удобно. если болшинство полей и свойств сделать статическими.
public class Player : MonoBehaviour
{
    public static Player Instance;

    public static int level;
    public static FNumber experience;
    public static FNumber[] experienceLevels;
    public static int freeStatPoints;
    public static FNumber gold;
    public static float Stamina;
    private static float staminaRegenerationRate = 20f; // В секунду при коэфициенте 1.
    public static float StaminaDrainPerAttack = 5f; // Сколько тратиться на обычный удар.
    public static float Mana;
    public static float damageDispersion = 0.1f; // Насколько урон может варьироваться.
    public static FNumber damageRecord;
    public static Spell currentSpell;
    public static bool isSpellAutocast = false;
    public static int maxLocationOpened;
    public static int lastPlayedLocationId;

    [Header("Player Stats")]
    public static int strength = 10;
    public static int dexterity = 10;
    public static int magicPower = 10;

    [Header("Balance Data")]
    public ExperienceTable experienceTable;
    public Weapon[] weapons;
    public Spell [] spells;

    
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        experienceLevels = experienceTable.levelRequirements; // shortcut for experience table.


        StartCoroutine("Regeneration");
    }

    public void InitializeNewPlayer()
    {
        // При начале новой игры сьросить все параметры игрока в начальные.
        level = 1;
        experience = new FNumber(0f);
        gold = new FNumber(0);
        strength = 10;
        dexterity = 10;
        magicPower = 10;
        freeStatPoints = 0;
        damageRecord = new FNumber(1f);
        Stamina = MaxStamina();
        Mana = MaxMana();
        maxLocationOpened = 1;
        lastPlayedLocationId = 1;
        currentSpell = null;
        foreach (var sp in spells)
        {
            sp.level = 0;
        }
        foreach (var w in weapons)
        {
            w.level = 0;
        }
        weapons[0].level = 1;
    }

    public void DrainStamina(Hit hit)
    {
        Stamina -= StaminaDrainPerAttack;
        if (Stamina <= 0f)
            Stamina = 0f;
    }

    public static void GainExperoence(FNumber exp)
    {
        // Получать опыт, пока не достиг максимального уровня.
        if (level < experienceLevels.Length)
        {
            experience += exp;
            while (experience >= experienceLevels[level])
            {
                level++;
                freeStatPoints++;
                Debug.Log("Congratulations! You get new experience level! " + level);
            }
            MenuController.Instance.RefreshExpreienceSlider();
        }
    }

    IEnumerator Regeneration()
    {
        int ticksPerSecond = 10; // Change here.
        // Восстановление ресурсов N раз в секунду.
        while (true)
        {
            // Stamina.
            //float staminaRemain = Stamina / MaxStamina();
            //float regenSpeedKoef;
            //if (staminaRemain >= 0.5f)
            //    regenSpeedKoef = 1f;
            //else
            //    regenSpeedKoef = staminaRemain * 1.5f + 0.25f;
            //Stamina += staminaRegenerationRate * regenSpeedKoef / ticksPerSecond;
            //if (Stamina >= MaxStamina())
            //    Stamina = MaxStamina();
            Stamina += staminaRegenerationRate / ticksPerSecond;
            if (Stamina >= MaxStamina())
                Stamina = MaxStamina();

            // Mana.
            Mana += ManaRegenerationRate() / ticksPerSecond;
            //Debug.Log("ManaRegen: " + ManaRegenerationRate / ticksPerSecond);
            if (Mana > MaxMana())
                Mana = MaxMana();
            //Debug.Log(1 / ticksPerSecond);
            yield return new WaitForSeconds((float)1 / ticksPerSecond);
        }
    }

    public enum State
    {
        KillingMobs,
        LurkingMenu
    }
    public static State currentState = State.KillingMobs;

    public FNumber Damage()
    {
        FNumber damage = new FNumber(0);

        // Берем урон из улучшений оружия.
        foreach (var w in weapons)
        {
            if (w.level > 0)
                damage = w.Damage();
        }

        // Добавляем влияние силы. 
        damage *= 1+(strength/50f); // по 2% за каждую силу.

        return damage;
    }

    public FNumber DamageWithDispersion()
    {
        return Damage()*Random.Range(1 - damageDispersion, 1 + damageDispersion);
    }

    public string DamageToString()
    {
        MutableString str = MutableString.Start.
            Add((Damage()*(1 - damageDispersion)).ToString()).
            Add("-").
            Add((Damage()*(1 + damageDispersion)).ToString());
        return str.ToString();
    }

    public Spell GetSpellById (int id)
    {
        for (int i = 0; i < spells.Length; i++)
        {
            if (spells[i].id == id)
                return spells[i];
        }
        return null;
    }

    // Вычисляет, какая сумма золота будет покупаться за 0.99$.
    public FNumber CurrentConsumable1()
    {
        return new FNumber (Mathf.Pow(10f, maxLocationOpened+2));
    }

    // Вычисляет, какая сумма золота будет покупаться за 2.49$.
    public FNumber CurrentConsumable2()
    {
        return new FNumber(Mathf.Pow(10f, maxLocationOpened + 2) * 5);
    }

    public static float CritChance()
    {
        return dexterity/2.5f;
    }

    public static float CritMultiplier()
    {
        return (strength*0.1f + dexterity*0.2f);
    }

    public static int MaxStamina()
    {
        return strength*10;
    }

    public static float MaxMana()
    {
        return magicPower*10f;
    }

    public static float ManaRegenerationRate()
    {
        return magicPower/2.5f;   
    }

    public static void UpgraideWeapon (Weapon weapon)
    {
        if (weapon.level < weapon.upgraides.Length && weapon.upgraides[weapon.level].cost <= gold)
        {
            gold -= weapon.upgraides[weapon.level].cost;
            weapon.level ++;
        }
    }

    public static void UpgraideSpell(Spell spell)
    {
        if (spell.level < spell.Upgraides().Length && spell.Upgraides()[spell.level].cost <= gold)
        {
            gold -= spell.Upgraides()[spell.level].cost;
            spell.level++;
        }
    }

    public void SavePlayerData()
    {
        PlayerData playerData = new PlayerData()
        {
            level = level,
            gold = gold,
            experience = experience,
            str = strength,
            dex = dexterity,
            wisdom = magicPower,
            damageRecord = damageRecord,
            freeStatsPoints = freeStatPoints,
            mana = Mana,
            stamina = Stamina,
            gameQuitTime = DateTime.Now.ToBinary(),
            locationOpenedId = maxLocationOpened
        };
        if (GameManager.Instance != null && GameManager.Instance.currLevel != null)
            playerData.locationPlayedId = GameManager.Instance.currLevel.id;
        else
            playerData.locationPlayedId = 1;
        if (currentSpell == null)
            playerData.currentSpellIndex = -1;
        else
            for (int i = 0; i < spells.Length ; i++)
                if (currentSpell = spells[i])
                    playerData.currentSpellIndex = i;
        playerData.spellsLevels = new int[spells.Length];
        for (int i = 0; i < spells.Length; i++)
            playerData.spellsLevels[i] = spells[i].level;
        playerData.weaponsLevels = new int[weapons.Length];
        for (int i = 0; i < weapons.Length; i++)
            playerData.weaponsLevels[i] = weapons[i].level;

        string Json = JsonUtility.ToJson (playerData);
        PlayerPrefs.SetString ("PlayerData", Json);
        PlayerPrefs.Save();
        Debug.Log("Player Data was saved successfully.");
    }

    public bool TryLoadGame()
    {
        if (PlayerPrefs.HasKey("PlayerData"))
        {
            string Json = PlayerPrefs.GetString("PlayerData");
            PlayerData playerData = JsonUtility.FromJson<PlayerData>(Json);

            level = playerData.level;
            gold = playerData.gold;
            experience = playerData.experience;
            strength = playerData.str;
            dexterity = playerData.dex;
            magicPower = playerData.wisdom;
            damageRecord = playerData.damageRecord;
            freeStatPoints = playerData.freeStatsPoints;
            Mana = playerData.mana;
            Stamina = playerData.stamina;
            if (playerData.currentSpellIndex == -1)
                currentSpell = null;
            else
                for (int i = 0; i < spells.Length; i++)
                    if (i == playerData.currentSpellIndex)
                        currentSpell = spells[i];
            for (int i = 0; i < weapons.Length; i++)
            {
                if (i < playerData.weaponsLevels.Length)
                    weapons[i].level = playerData.weaponsLevels[i];
                else weapons[i].level = 0;
            }
            for (int i = 0; i < spells.Length; i++)
            {
                if (i < playerData.spellsLevels.Length)
                    spells[i].level = playerData.spellsLevels[i];
                else spells[i].level = 0;
            }
            DateTime lastPlayedTime = DateTime.FromBinary (playerData.gameQuitTime);
            TimeSpan timeElapsed = DateTime.Now - lastPlayedTime;
            Mana += (float) (ManaRegenerationRate() * timeElapsed.TotalSeconds);
            Stamina += (float) (staminaRegenerationRate * timeElapsed.TotalSeconds);
            maxLocationOpened = playerData.locationOpenedId;
            lastPlayedLocationId = playerData.locationPlayedId;
            // LOAD LAST PLAYER LOCATION.
            Debug.Log("Player Data was loaded successfully.");
            return true;
        }
        return false;
    }
}

// Используется для сохранения и загрузки данных игрока.
[System.Serializable]
public class PlayerData
{
    public int level;
    public FNumber gold;
    public FNumber experience;
    public int str;
    public int dex;
    public int wisdom;
    public FNumber damageRecord;
    public int freeStatsPoints;
    public float mana;
    public float stamina;
    public int currentSpellIndex;
    public int[] spellsLevels;
    public int[] weaponsLevels;
    public long gameQuitTime;
    public int locationPlayedId;
    public int locationOpenedId;
}






