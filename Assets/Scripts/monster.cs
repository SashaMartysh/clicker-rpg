﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using MarchingBytes; // Easy Pool

public class Monster : MonoBehaviour
{
    [Header("Main")] public int id;
    public string Name;
    public Type type;
    public FNumber goldReward;
    [HideInInspector] public bool isDead = false;

    public bool isInvulnerable = false;

    [Header("Stats")]
    public int dexterity;
    public float regenerationRate; // В процентах от максимального здоровья.
    public int regeneration;
    public FNumber maxHealth;
    [HideInInspector] public FNumber health;
    public float healthBasedOnLevel;

    [Header("Resistances")]
    public float armor; // %
    public float fireResist;
    public float coldResist;
    public float lightingResist;

    [Header("Elements")]
    public Animator animator;
    public AudioSource audioSource;

    private float soundPitchDispersion = 0.15f;

    // для рассылки сообщения о том, что монстр отбросил копыта! :)
    public delegate void MobIsDeadDelegate(Monster monster);
    public event MobIsDeadDelegate mobIsDeadEvent;

    void Start()
    {
        if (healthBasedOnLevel > 0)
        {
            maxHealth = new FNumber(Mathf.Pow(10f, GameManager.Instance.currLevel.id)*healthBasedOnLevel);
            //maxHealth = new FNumber(1f);
            // Здоровье монстра зависит от номера уровня и некоего множителя.
        }
        health = maxHealth;

        goldReward = new FNumber(Mathf.Pow(10f, GameManager.Instance.currLevel.id - 1));
        goldReward *= Random.Range(0.9f, 1.1f);
        goldReward *= (1f - (GameManager.Instance.currLevel.id*0.01f));

        // подписываем игровые менеджеры ня новости о смертях монстров
        mobIsDeadEvent += GameManager.Instance.MonsterDeathRegistered;
    }

    void OnMouseDown()
    {
        if (isDead)
            return;
        if (MenuController.Instance.IsSomeWindowOpened())
            return;
        if (Player.Stamina < Player.StaminaDrainPerAttack)
            {
                MenuController.Instance.NotEnoughtStaminaAnimation();
                return;
            }
        Hit hit = GenerateNewMeleeHit();
        Player.Instance.DrainStamina(hit);
        if (hit.dmg > Player.damageRecord)
                Player.damageRecord = hit.dmg;
        TakeHit(hit);
        PlaySound(hit);

    }

    public void TakeHit(Hit hit)
    {
        if (hit.isSuccess)
        {
            health -= hit.dmg;
            animator.SetTrigger("IsClicked");
            MenuController.Instance.RefreshEnemyHealthBar();
            if (health <= 0)
            {
                isDead = true;
                GameManager.Instance.lastDeathTime = Time.realtimeSinceStartup;
                Player.GainExperoence(maxHealth);
                // создаем popup (всплывающий значок) с обозначением количества полученного золота
                Vector3 position = new Vector3(0, UnityEngine.Random.Range(2f, 3f), 0);
                GameObject coin = EasyObjectPool.instance.GetObjectFromPool("CoinsPool", position, Quaternion.identity);
                coin.GetComponent<Coin>().RepresentCoin (goldReward);
                DeathAnimation();
            }
        }
        else
        {
            ShowEscapeAnimation();
        }
        DamagePopup.PopupGamage(hit);
    }

    private void ShowEscapeAnimation()
    {
        // Если монстр увернулся от удара, то он отпрыгивает вправо или влево.
        if (type == Type.ground)
        {
            if (transform.position.x < GameManager.Instance.currLevel.centralGroundPoint.position.x)
            {
                transform.position = GameManager.Instance.currLevel.rightEscapeGroundPoint.position;
                FaceToLeft();
            }
            else
            {
                transform.position = GameManager.Instance.currLevel.leftEscapeGroundPoint.position;
                FaceToRight();
            }
        }
        else
        {
            if (transform.position.x < GameManager.Instance.currLevel.centralGroundPoint.position.x)
            {
                transform.position = new Vector3(GameManager.Instance.currLevel.rightEscapeFlyPoint.position.x,
                    transform.position.y,
                    0f);
                FaceToLeft();
            }
            else
            {
                transform.position = new Vector3(GameManager.Instance.currLevel.leftEscapeFlyPoint.position.x,
                    transform.position.y,
                    0f);
                FaceToRight();
            }
        }

    }

    private void DeathAnimation()
    {
        //transform.localRotation = Quaternion.Euler(0f, 0f, Random.Range(30f, 80f));
        LeanTween.rotateZ(gameObject, Random.Range(30f, 80f), 0.25f);
        LeanTween.move(gameObject, new Vector3(0f, -10f, 0f), 1f).setEase(LeanTweenType.easeInSine);
        Destroy(gameObject, 1f);
    }

    private Hit GenerateNewMeleeHit()
    {
        // проверяем, не промазал ли игрок?
        if (Random.Range(0.0f, 1.0f) < (float) (dexterity*dexterity)/(dexterity*dexterity + Player.dexterity*Player.dexterity)) // монстр увернулся
            return new Hit(false, new FNumber(0, FNumber.Rank.units), false);

        // проверяем, выпадает ли критический удар?
        FNumber damage = (Player.Instance.DamageWithDispersion()*(1 - armor));
        if (damage < 1f)
            damage = new FNumber(1, FNumber.Rank.units);
        if (UnityEngine.Random.Range(0, 100) <= Player.CritChance()) // isCritical! :)
            return new Hit(true, damage*Player.CritMultiplier(), true);
        else // standart damage :(
            return new Hit(true, damage, false);
    }

    void PlaySound(Hit hit)
    {
        if (hit.isSuccess == false)
        {
            audioSource.clip = SoundController.Instance.miss;
            audioSource.pitch = Random.Range(1f - soundPitchDispersion, 1f + soundPitchDispersion);
        }
        else
        {
            // звук получения удара
            if (hit.isCritical)
                audioSource.pitch = 0.8f;
            else
                audioSource.pitch = Random.Range(1f - soundPitchDispersion, 1f + soundPitchDispersion);
            audioSource.clip = SoundController.Instance.hit;
        }
        audioSource.Play();
    }

    public void popupMagicHit(ulong damage)
    {
        // местоположение попапа на экране
        Vector3 position = new Vector3((float) UnityEngine.Random.Range(-200, 200)/100,
            (float) UnityEngine.Random.Range(0, 300)/100,
            0);
        //GameObject popupDamage = Instantiate(GameManager.Instance.popupDamage, position, Quaternion.identity);
        //popupDamage.GetComponent<TextMesh>().color = Color.blue;
        // popupDamage.GetComponent<TextMesh>().text = damage.ToString();
    }

    public enum Type
    {
        ground = 1,
        fly = 2
    }

    public void FaceToLeft()
    {
        transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y,
            transform.localScale.z);
    }

    public void FaceToRight()
    {
        transform.localScale = new Vector3(-1f*Mathf.Abs(transform.localScale.x), transform.localScale.y,
            transform.localScale.z);
    }

    IEnumerator HealthRegeneration()
    {
        while (true)
        {
            //if (!isDead)
            //{
            //    if (maxHealth < 1000)
            //    {
            //        health += (uint) regeneration;
            //    }
            //    else
            //    {
            //        health += (ulong)(regenerationRate*maxHealth);
            //    }
            //}
            yield return new WaitForSeconds(1f);
        }
    }
}

[System.Serializable]
public class MonsterWithChance
{
    public GameObject monster;
    public int chance;
}
