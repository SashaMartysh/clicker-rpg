﻿using UnityEngine;

// Представляет собой тип рукопашного удара игрока по монстру.
[CreateAssetMenu(menuName = "Weapon")]
public class Weapon : ScriptableObject
{
    public string name;
    public int level;
    public WeaponUpgraide[] upgraides;

    public FNumber Damage()
    {
        if (level > 0)
            return upgraides[level - 1].damage;
        else return new FNumber(0f);
    }
}

[System.Serializable]
public struct WeaponUpgraide
{
    public FNumber damage;
    public FNumber cost;
}

// DONT NEED IT. DELETE.
//public enum WeaponType
//{
//    wooden = 1,
//    bronze = 2,
//    silver = 3,
//    gold = 4
//}