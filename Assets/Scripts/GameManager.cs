﻿using System.Collections;
using UnityEngine;
using MarchingBytes; // Easy Pool - for game object pool

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public bool isSpawnAllowed = true;
    [HideInInspector] public bool isSlowmoAfterCriticalHit = false;
    private double lastSpawnTime;
    public State currentState = State.MainMenu;

    [HideInInspector]
    public Level currLevel;
    [HideInInspector] public float lastDeathTime; // Момент смерти последнего врага.

    //public Transform clickPosition;


    void Awake()
    {
        MakeSingleton();
    }

    void Update()
    {
        //if (Input.touchCount > 0)
        //{
        //    clickPosition.position = Input.GetTouch(0).position;
        //}

    }

    void FixedUpdate()
    {
        if (isSlowmoAfterCriticalHit)
        {
            //Debug.Log("TimeScale: " + Time.timeScale);
            if (Time.timeScale < 1f)
            {
                Time.timeScale += 0.01f;
                if (Time.timeScale > 1.0f)
                    Time.timeScale = 1.0f;
            }
            else
            {
                isSlowmoAfterCriticalHit = false;
            }
        }
    }

    private void MakeSingleton()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    // события, подписанное на смерть моба, получает сигнал при смерти очередной твари
    public void MonsterDeathRegistered (Monster enemy)
    {

    }
    
    void OnApplicationQuit()
    {
        Player.Instance.SavePlayerData();
        MenuController.Instance.SavePreferences();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Player.Instance.SavePlayerData();
            MenuController.Instance.SavePreferences();
        }
    }

    IEnumerator MagicAutoCasting()
    {
        while (true)
        {
            // Случаи, когда кастовать не надо:
            if (MenuController.Instance.IsSomeWindowOpened())
                goto skipCast;
            if (Player.currentSpell == null)
                goto skipCast;
            if (Player.currentSpell.level < 1)
                goto skipCast;
            if (Player.currentSpell.ManaCost() > Player.Mana)
                goto skipCast;
            if (Player.currentSpell.isNeedTargetForCast && currLevel.currMonster == null)
                goto skipCast;

            // Else Cast!!!
            Player.Mana -= Player.currentSpell.ManaCost();
            EasyObjectPool.instance.GetObjectFromPool(Player.currentSpell.name + "Pool", transform.position, Quaternion.identity);
            //Instantiate(Player.currentSpell.spellEffect, transform.position, Quaternion.identity);

            skipCast:
            yield return new WaitForSeconds(Player.currentSpell.castInterval);
        }
    }

    public void AutoCastStart()
    {
        StartCoroutine("MagicAutoCasting");
    }

    public void AutoCastStop()
    {
        StopCoroutine("MagicAutoCasting");
    }

    public enum State
    {
        Game = 1,
        MainMenu = 2
    }

    public void ClearLocation()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Mob");
        for (int i = 0; i < enemies.Length; i++)
        {
            Destroy(enemies[i].gameObject);
        }
        GameObject[] spellEffects = GameObject.FindGameObjectsWithTag("SpellEffect");
        for (int i = 0; i < spellEffects.Length; i++)
        {
            Destroy(spellEffects[i].gameObject);
        }
    }
}
