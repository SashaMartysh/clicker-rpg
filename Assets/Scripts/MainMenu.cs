﻿using UnityEngine;


public class MainMenu : MonoBehaviour {

    [Header("MainMenu")]
    public GameObject gameContinueButton;

    void OnEnable()
    {
        RefreshMenu();
    }

    public void RefreshMenu()
    {
        if (PlayerPrefs.HasKey("PlayerData"))
            gameContinueButton.SetActive(true);
        else
            gameContinueButton.SetActive(false);
    }

    public void StartNewGame()
    {
        gameObject.SetActive(false);
        Player.Instance.InitializeNewPlayer();
        GameManager.Instance.currentState = GameManager.State.Game;
        LevelController.Instance.GoToLevel (LevelController.Instance.GetLevelById(1));
    }

    public void ContinueGame()
    {
        gameObject.SetActive(false);

        if (Player.Instance.TryLoadGame() == false)
            Player.Instance.InitializeNewPlayer();
        GameManager.Instance.currentState = GameManager.State.Game;
        LevelController.Instance.GoToLevel (LevelController.Instance.GetLevelById(Player.lastPlayedLocationId));
    }
}
