﻿using System;
using System.Collections.Generic;
using SmartLocalization;
using UnityEngine;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public static MenuController Instance;

    [Header("Opened Windows")]
    public List<GameWindow> openedWindows;
    
    [Header("Options")]
    public bool isSoundAllowed = true;
    public float soundsVolume = 0f;
    public Toggle soundToggle;
    public Slider soundVolumeSlider;
    public AudioMixer audioMixer;

    [Header("Game Screen Elements")]
    public TextMeshProUGUI goldLabel;
    public Slider enemyHealthBar;
    public Slider experienceSlider;
    public Slider staminaSlider;
    public Image staminaWarning;
    public Slider manaSlider;
    public Image magicAutoCastButtonImage;

    [Header("Character Screen Elements")]
    public GameObject CharacterPanel;
    public TextMeshProUGUI level;
    public TextMeshProUGUI experience;
    public TextMeshProUGUI infoDmgValue;
    public TextMeshProUGUI infoCritChanceValue;
    public TextMeshProUGUI infoCritMultiplierValue;
    public TextMeshProUGUI stamina;
    public TextMeshProUGUI freeStatspoints;
    public TextMeshProUGUI maxMana;
    public TextMeshProUGUI manaRegenerationRate;
    public TextMeshProUGUI damageRecord;

    [Header("Magic Screen")]
    public GameObject magicScreen;
    public TextMeshProUGUI currentSpell;
    public Image currentSpellImage;

    [Header("Weapon Upgraide Buttons")]
    public WeaponUpgraideIcon [] weaponUpgradeButtons;

    [Header("STR DEX WSDM")]
    public TextMeshProUGUI strengthValue;
    public Slider strengthSlider;
    public TextMeshProUGUI dexterityValue;
    public Slider dexteritySlider;
    public TextMeshProUGUI wisdomValue;
    public Slider wisdomSlider;
    public GameObject[] statsUpgraideButtons;

    [Header("Other")]
    public GameObject QuitPanel;
    public GameObject mapPanel;
    public GameObject optionsScreen;
    public GameObject startMenuScreen;
    public GameObject travelingScreen;
    public Animator magicAutoCastAnimator;
    public GameObject mainMenu;
    public GameObject inGamePurchasesPanel;

    void Awake()
    {
        SetupSingleton();
        openedWindows = new List<GameWindow>();
        //LanguageManager.Instance.ChangeLanguage("uk");
    }

    void Start()
    {
        //RefreshExpreienceSlider();
        RefreshInGameUI();
        LoadPreferences();
    }
    
    void Update()
    {
        DrawPlayerGold();
        RefreshSliders();
    }

    public bool IsSomeWindowOpened()
    {
        if (openedWindows.Count > 0)
            return true;
        return false;
    }

    public void RefreshSliders()
    {
        staminaSlider.maxValue = Player.MaxStamina();
        staminaSlider.value = Player.Stamina;

        manaSlider.maxValue = Player.MaxMana();
        manaSlider.value = Player.Mana;
    }

    public void RefreshEnemyHealthBar()
    {
        if (Level.Instance.currMonster != null)
        {
            enemyHealthBar.maxValue = Level.Instance.currMonster.maxHealth.ToFloat();
            enemyHealthBar.value = Level.Instance.currMonster.health.ToFloat();
        }
        else
        {
            enemyHealthBar.value = 0f;
        }
    }

    public void RefreshExpreienceSlider()
    {
        if (Player.level < Player.experienceLevels.Length)
        {
            experienceSlider.maxValue = (Player.experienceLevels[Player.level] - Player.experienceLevels[Player.level - 1]).ToFloat();
            experienceSlider.value = (Player.experience - Player.experienceLevels[Player.level - 1]).ToFloat();
        }
        else experienceSlider.gameObject.SetActive(false);
    }

    public void OpenCloseCharacterPanel()
    {
        if (CharacterPanel.activeSelf)
        {
            CharacterPanel.SetActive(false);
        }
        else 
        {
            for (int i = 0; i < openedWindows.Count; i++)
                openedWindows[i].gameObject.SetActive(false);
            CharacterPanel.SetActive(true);
            RefreshCharacterScreen();
        }
    }

    public void OpenCloseMap()
    {
        if (mapPanel.activeSelf)
            mapPanel.SetActive(false);
        else mapPanel.SetActive(true);
    }

    public void OpenCloseOptions()
    {
        if (optionsScreen.activeSelf)
            optionsScreen.SetActive(false);
        else
        {
            for (int i = 0; i < openedWindows.Count; i++)
                openedWindows[i].gameObject.SetActive(false);
            optionsScreen.SetActive(true);
            RefreshOptionsScreen();
        }
    }

    //public void OpenCloseStartMenu()
    //{
    //    if (startMenuScreen.activeSelf)
    //        startMenuScreen.SetActive(false);
    //    else startMenuScreen.SetActive(true);
    //}

    void DrawPlayerGold()
    {
        goldLabel.text = Player.gold.ToString() + "<sprite=0>";
    }

    public void OpenCloseQuitMenu()
    {
        if (QuitPanel.activeSelf)
        {
            QuitPanel.SetActive(false);
        }
        else
        {
            QuitPanel.SetActive(true);
        }
    }

    public void OpenMainMenu()
    {
        Player.Instance.SavePlayerData();
        GameManager.Instance.ClearLocation();
        MagicAutoCastStop();
        for (int i = 0; i < LevelController.Instance.levels.Length; i++)
        {
            LevelController.Instance.levels[i].gameObject.SetActive(false);
        }
        GameManager.Instance.currentState = GameManager.State.MainMenu;
        for (int i = 0; i < openedWindows.Count; i++)
            openedWindows[i].gameObject.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void OpenCloseMagicScreen()
    {
        if (magicScreen.activeSelf)
        {
            magicScreen.SetActive(false);
        }
        else
        {
            for (int i = 0; i < openedWindows.Count; i++)
                openedWindows[i].gameObject.SetActive(false);
            magicScreen.SetActive(true);
            RefreshMagicScreen();
        }
    }

    public void OpenInGamePurchasesScreen()
    {
        inGamePurchasesPanel.SetActive(true);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void CancelQuit()
    {
        QuitPanel.SetActive(false);
        Player.currentState = Player.State.KillingMobs;
    }

    public void ShowTravelingScreen()
    {
        travelingScreen.SetActive(true);
    }

    public void HideTravelingScreen()
    {
        travelingScreen.SetActive(false);
    }

    public void RefreshCharacterScreen()
    {
        level.text = LanguageManager.Instance.GetTextValue("L_Level") + ": " + Player.level.ToString();
        experience.text = LanguageManager.Instance.GetTextValue("L_Experience") + ": " + Player.experience.ToString();
        infoDmgValue.text = Player.Instance.DamageToString();
        infoCritChanceValue.text = Player.CritChance().ToString() + "%";
        infoCritMultiplierValue.text = Player.CritMultiplier().ToString() + "x";
        stamina.text = Player.MaxStamina().ToString();
        maxMana.text = Player.MaxMana().ToString();
        manaRegenerationRate.text = Player.ManaRegenerationRate().ToString() + LanguageManager.Instance.GetTextValue("L_/sec");
        damageRecord.text = Player.damageRecord.ToString();
        strengthValue.text = Player.strength.ToString();
        dexterityValue.text = Player.dexterity.ToString();
        wisdomValue.text = Player.magicPower.ToString();
        {
            int maxStat = Mathf.Max(Player.strength, Player.dexterity, Player.magicPower) + 5;
            strengthSlider.maxValue = maxStat;
            dexteritySlider.maxValue = maxStat;
            wisdomSlider.maxValue = maxStat;
            strengthSlider.value = Player.strength;
            dexteritySlider.value = Player.dexterity;
            wisdomSlider.value = Player.magicPower;
        }
        if (Player.freeStatPoints > 0)
        {
            freeStatspoints.gameObject.SetActive(true);
            freeStatspoints.text = LanguageManager.Instance.GetTextValue("L_FreeStatsPoints") + ":" +
                                   Player.freeStatPoints;
            foreach (var btn in statsUpgraideButtons)
                btn.SetActive(true);
        }
        else
        {
            freeStatspoints.gameObject.SetActive(false);
            foreach (var btn in statsUpgraideButtons)
                btn.SetActive(false);
        }

        for (int i = 0; i < weaponUpgradeButtons.Length; i++)
        {
            // Отключать оружие, которое уже устарело.
            if (i > 0 && weaponUpgradeButtons[i].weapon.level > 5)
                weaponUpgradeButtons[i-1].gameObject.SetActive(false);
            weaponUpgradeButtons[i].RefreshIcon();
        }
    }

    public void RefreshMagicScreen()
    {
        if (Player.currentSpell == null)
            currentSpellImage.gameObject.SetActive(false);
        else
        {
            currentSpellImage.gameObject.SetActive(true);
            currentSpellImage.sprite = Player.currentSpell.sprite;
            currentSpell.text = LanguageManager.Instance.GetTextValue("L_CurrentSpell") + ": " +
                    LanguageManager.Instance.GetTextValue(Player.currentSpell.name);
        }
    }

    public void RefreshInGameUI()
    {
        if (Player.currentSpell == null)
            magicAutoCastButtonImage.color = Color.gray;
        else
            magicAutoCastButtonImage.color = MyColors.Gold();
    }

    private void SetupSingleton()
    {
        if (Instance == null)
            Instance = this;
        else Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    // It is like event, when any windows changes its state.
    public void OnSomeWindowOpenClose()
    {
        if (IsSomeWindowOpened())
            Time.timeScale = 0f;
        else Time.timeScale = 1f;
    }

   public void ChangeLocalizationToRussian()
    {
        LanguageManager.Instance.ChangeLanguage("ru");
    }

    public void ChangeLocalizationToUkrainiann()
    {
        LanguageManager.Instance.ChangeLanguage("uk");
    }

    public void ChangeLocalizationToEnglish()
    {
        LanguageManager.Instance.ChangeLanguage("en");
    }

    public void StartnewGame()
    {
        LevelController.Instance.GoToLevel(LevelController.Instance.levels[0]);
    }

    public void UpPlayerStat(int id)
    {
        Player.freeStatPoints--;
        if (id == 0)
            Player.strength++;
        else if (id == 1)
            Player.dexterity++;
        else if (id == 2)
            Player.magicPower++;
        RefreshCharacterScreen();
    }

    public void NotEnoughtStaminaAnimation()
    {
        staminaWarning.color = Color.red;
        Invoke("ReturnStaminaSilderColor", 0.25f);
    }

    private void ReturnStaminaSilderColor()
    {
        staminaWarning.color = MyColors.Grey();
    }

    public void SwitchSpellAutocastOnOff()
    {
        if (Player.isSpellAutocast == false && Player.currentSpell != null)
            MagicAutoCastStart();
        else
            MagicAutoCastStop();
    }

    public void MagicAutoCastStart()
    {
        GameManager.Instance.AutoCastStart();
        magicAutoCastAnimator.SetBool("isAutoCasting", true);
        Player.isSpellAutocast = true;
    }

    public void MagicAutoCastStop()
    {
        GameManager.Instance.AutoCastStop();
        magicAutoCastAnimator.SetBool("isAutoCasting", false);
        Player.isSpellAutocast = false;
    }

    public void SwitchSoundsOnOff()
    {
        isSoundAllowed = soundToggle.isOn;
        if (isSoundAllowed == false)
            audioMixer.SetFloat("sfxVolume", -80f);
        else audioMixer.SetFloat("sfxVolume", soundsVolume);
    }

    public void VolumeChange (float volume)
    {
        soundsVolume = volume;
        if (isSoundAllowed)
        {
            audioMixer.SetFloat("sfxVolume", volume);
        }
    }

    public void RefreshOptionsScreen()
    {
        soundVolumeSlider.value = soundsVolume;
        soundToggle.isOn = isSoundAllowed;
    }

    void LoadPreferences()
    {
        if (PlayerPrefs.HasKey("soundVolume"))
        {
            soundsVolume = PlayerPrefs.GetFloat("soundVolume");
            soundVolumeSlider.value = soundsVolume;
        }
        if (PlayerPrefs.HasKey("soundAllowed"))
            isSoundAllowed = PlayerPrefs.GetInt("soundAllowed") == 1 ? true : false;
        if (isSoundAllowed)
            audioMixer.SetFloat("sfxVolume", soundsVolume);
        else audioMixer.SetFloat("sfxVolume", -80f);

        if (PlayerPrefs.HasKey("language"))
            LanguageManager.Instance.ChangeLanguage(PlayerPrefs.GetString("language"));
        else
        {
            if (LanguageManager.Instance.GetSystemLanguageEnglishName().StartsWith("en")
                || LanguageManager.Instance.GetSystemLanguageEnglishName().StartsWith("En"))
                LanguageManager.Instance.ChangeLanguage("en");
            else if (LanguageManager.Instance.GetSystemLanguageEnglishName().StartsWith("ru")
                || LanguageManager.Instance.GetSystemLanguageEnglishName().StartsWith("Ru"))
                LanguageManager.Instance.ChangeLanguage("ru");
            else LanguageManager.Instance.ChangeLanguage("uk");
        }
    }

    public void SavePreferences()
    {
        PlayerPrefs.SetFloat("soundVolume", soundsVolume);
        PlayerPrefs.SetInt("soundAllowed", isSoundAllowed ? 1 : 0);
        PlayerPrefs.SetString("language", LanguageManager.Instance.LoadedLanguage);
        PlayerPrefs.Save();
    }

}
