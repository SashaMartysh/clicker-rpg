﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance;

    [Tooltip("List of all game levels.")]
    public Level[] levels;
    private Level destinationPlace;

    void Awake()
    {
        MakeSingleton();
    }

    public Level GetLevelById (int id)
    {
        for (int i = 0; i < levels.Length; i++)
        {
            if (levels[i].id == id)
                return levels[i];
        }
        Debug.Log("ERROR: Level was searched by unexisted ID.");
        return null;
    }

    // Начать переключение уровня. Включить заставку путешествия.
    public void GoToLevel(Level level)
    {
        MenuController.Instance.mapPanel.SetActive(false);
        MenuController.Instance.ShowTravelingScreen();

        //if (GameManager.Instance.currMonster != null)
        //    Destroy(GameManager.Instance.currMonster.gameObject);
        GameManager.Instance.ClearLocation();
        MenuController.Instance.MagicAutoCastStop();
        //GameManager.Instance.currMonsterObject = null;
        //GameManager.Instance.currMonster = null;
        foreach (var l in levels)
        {
            l.gameObject.SetActive(false);
        }
        destinationPlace = level;

        Invoke("StartLevel", 1.5f);
    }

    // Закончить переключение уровня. Убрать заставку перемещения.
    private void StartLevel()
    {
        GameManager.Instance.currLevel = destinationPlace;
        destinationPlace.gameObject.SetActive(true);
    }

    private void MakeSingleton()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
}
