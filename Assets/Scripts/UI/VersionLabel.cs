﻿using UnityEngine;
using TMPro;

public class VersionLabel : MonoBehaviour
{
    public TextMeshProUGUI label;

    void Awake()
    {
        label.text = "Version: " + Application.version;
    }
}
