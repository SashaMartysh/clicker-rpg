﻿using SmartLocalization;
using UnityEngine;
using TMPro;

public class WeaponUpgraideIcon : MonoBehaviour
{
    public Weapon weapon;

    [Header("Elements")]
    public TextMeshProUGUI labelWeaponName;

    [Header("Upgraide Panel")]
    public GameObject upgraidePanel;
    public TextMeshProUGUI level;
    public TextMeshProUGUI currentDamage;
    public TextMeshProUGUI nextDamage;
    public TextMeshProUGUI upgraideCost;

    [Header("Research Panel")]
    public GameObject researchPanel;
    public TextMeshProUGUI researchCost;

    [Header("Completed Panel")]
    public GameObject completedPanel;

    void Start()
    {
        LanguageManager.Instance.OnChangeLanguage += OnChangeLanguage;
        RefreshIcon();
    }

    void OnChangeLanguage(LanguageManager languageManager)
    {
        RefreshIcon();
    }

    void OnEnable()
    {
        RefreshIcon();
    }

    // Обновляет информацию на табличке.
    public void RefreshIcon ()
    {
        labelWeaponName.text = LanguageManager.Instance.GetTextValue(weapon.name);

        // Если оружие ще не изучено (т.е. уровень = 0), то отрисовать панель для изучения оружия.
        if (weapon.level == 0)
        {
            upgraidePanel.SetActive(false);
            completedPanel.SetActive(false);
            researchPanel.SetActive(true);

            researchCost.text = weapon.upgraides[0].cost.ToString() + "<sprite=0>";
        }
        else
        {
            // Иначе, если оружие уже изучено и его можно улучшать.
            if (weapon.level < weapon.upgraides.Length)
            {
                researchPanel.SetActive(false);
                completedPanel.SetActive(false);
                upgraidePanel.SetActive(true);
                level.text = weapon.level.ToString();
                currentDamage.text = LanguageManager.Instance.GetTextValue("L_Damage") + ": " + weapon.upgraides[weapon.level - 1].damage.ToString();
                nextDamage.text = LanguageManager.Instance.GetTextValue("L_OnNextLevel") + ": " + weapon.upgraides[weapon.level].damage.ToString();
                upgraideCost.text = weapon.upgraides[weapon.level].cost.ToString() + "<sprite=0>";
            }
            else
            {
                // Если оружие изучено уже до максимального уровня, то показать это.
                researchPanel.SetActive(false);
                upgraidePanel.SetActive(false);
                completedPanel.SetActive(true);
            }
        }
    }

    public void Upgraide()
    {
        Player.UpgraideWeapon(weapon);
        RefreshIcon();
        MenuController.Instance.RefreshCharacterScreen();
    }
}
