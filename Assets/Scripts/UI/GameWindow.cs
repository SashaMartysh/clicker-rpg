﻿using UnityEngine;

public class GameWindow : MonoBehaviour {

    public bool isGamePlayStop = true;

    void OnEnable()
    {
        //Debug.Log(gameObject.name + " is opened.");
        if (MenuController.Instance != null)
        {
            MenuController.Instance.openedWindows.Add(this);
            MenuController.Instance.OnSomeWindowOpenClose();
        }
    }

    void OnDisable()
    {
        //Debug.Log(gameObject.name + " is closed.");
        if (MenuController.Instance != null)
        {
            MenuController.Instance.openedWindows.Remove(this);
        MenuController.Instance.OnSomeWindowOpenClose();
        }
    }
}
