﻿using SmartLocalization;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using SmartLocalization;

public class SpellUpgraideCard : MonoBehaviour
{
    [Header("Main")]
    public Spell spell;

    [Header("Elements")]
    public TextMeshProUGUI name;
    public Image image;

    [Header("Upgraide Panel")]
    public GameObject upgraidePanel;
    public TextMeshProUGUI level;
    public TextMeshProUGUI currentDamage;
    public TextMeshProUGUI nextDamage;
    public TextMeshProUGUI upgraideCost;

    [Header("Research Panel")]
    public GameObject researchPanel;
    public TextMeshProUGUI researchCost;

    [Header("Completed Panel")]
    public GameObject completedPanel;

    void Start()
    {
        LanguageManager.Instance.OnChangeLanguage += OnChangeLanguage;
    }

    void OnEnable()
    {
        RefreshIcon();
    }

    void OnChangeLanguage(LanguageManager languageManager)
    {
        RefreshIcon();
    }

    // Обновляет информацию на табличке.
    public void RefreshIcon ()
    {
        name.text = LanguageManager.Instance.GetTextValue(spell.name);
        image.sprite = spell.sprite;

        // Если заклинание еще не изучено:
        if (spell.level == 0)
        {
            upgraidePanel.SetActive(false);
            completedPanel.SetActive(false);
            researchPanel.SetActive(true);

            researchCost.text = spell.Upgraides()[0].cost.ToString() + "<sprite=0>";
        }
        else
        {
            // Иначе, если уже изучено и его можно улучшать.
            if (spell.level < spell.Upgraides().Length)
            {
                researchPanel.SetActive(false);
                completedPanel.SetActive(false);
                upgraidePanel.SetActive(true);
                level.text = spell.level.ToString();
                currentDamage.text = LanguageManager.Instance.GetTextValue("L_Damage") + ": " + spell.Upgraides()[spell.level - 1].damage.ToString();
                nextDamage.text = LanguageManager.Instance.GetTextValue("L_OnNextLevel") + ": " + spell.Upgraides()[spell.level].damage.ToString();
                upgraideCost.text = spell.Upgraides()[spell.level].cost.ToString() + "<sprite=0>";
            }
            else
            {
                // Если изучено уже до максимального уровня, то показать это.
                researchPanel.SetActive(false);
                upgraidePanel.SetActive(false);
                completedPanel.SetActive(true);
            }
        }
    }

    public void SelectOrUpgraide()
    {
        // Первый щелчок выбирает заклинание и делает его "нынешним", второй и далее щелчки улучшают заклинание.
        if (spell.level > 0 && Player.currentSpell != spell)
        {
            Player.currentSpell = spell;
        }
        else
        {
            Player.UpgraideSpell(spell);
        }
        RefreshIcon();
        MenuController.Instance.RefreshMagicScreen();
        MenuController.Instance.RefreshInGameUI();
    }
}
