﻿using System.Collections;
using UnityEngine;

public class LevelWithBoss : Level
{
    [Header("Boss Level")]
    public GameObject messageScreen;


    public override void StartGamePlay()
    {
        SpawnEnemies();
        StartCoroutine("MonitoringBoss");
    }

    IEnumerator MonitoringBoss()
    {
        while (true)
        {
            if (currMonster == null && Time.realtimeSinceStartup - GameManager.Instance.lastDeathTime >= 2f)
                EndGame();
           yield return new WaitForSeconds(0.1f);
        }
    }

    void EndGame()
    {
        StopCoroutine("MonitoringBoss");
        GameManager.Instance.AutoCastStop();
        messageScreen.SetActive(true);
        gameObject.SetActive(false);
    }
}
