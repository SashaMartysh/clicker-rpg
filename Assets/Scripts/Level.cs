﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Level : MonoBehaviour
{
    public static Level Instance;

    [Header("Main")]
    public int id;
    public string name;
    [Tooltip("Задержка перед появлением нового врага после смерти предыдущего.")]
    public float spawnDelay = 0.5f;
    public MonsterWithChance[] monstersPool;

    [Header("Spawns points")]
    public Transform leftGroundSpawn;
    public Transform rightGroundSpawn;
    public Transform centralGroundPoint;
    public Transform leftFlySpawn;
    public Transform rightFlySpawn;
    public Transform centralFlyPoint;
    public Transform outerPoint; // for "preSpawn"
    [Header("Escape points")]
    public Transform leftEscapeGroundPoint;
    public Transform rightEscapeGroundPoint;
    public Transform leftEscapeFlyPoint;
    public Transform rightEscapeFlyPoint;
    [Header("Camera Points")]
    public bool startDemo = false;
    private GameObject camera;
    public Transform cameraStartPosition;   // Для созданий мини-заставки в начале уровня.
    public Transform cameraFinalPosition;
    [Header("Standart Points")]
    public Level standartPoints;

    [HideInInspector] public Monster currMonster;
    int enemiesSpawned = 0;


    void OnEnable()
    {
        SetupPoints();
        Instance = this;
        Invoke("OnLevelStart", 0.5f);
    }

    void OnDisable()
    {
        if (GameManager.Instance.currLevel == this)
            GameManager.Instance.currLevel = null;
    }

    public void OnLevelStart()
    {
        GameManager.Instance.currLevel = this;
        MenuController.Instance.HideTravelingScreen();

        if (startDemo)
        {
            camera = Camera.main.gameObject;
            camera.transform.position = cameraStartPosition.position;
            LeanTween.move(camera, cameraFinalPosition.position, 0.5f).setOnComplete(StartGamePlay);
        }
        else StartGamePlay();
    }

    public virtual void StartGamePlay()
    {
        GameManager.Instance.isSpawnAllowed = true;
        GameManager.Instance.lastDeathTime = Time.realtimeSinceStartup;
        StartCoroutine("Spawning");
    }

    IEnumerator Spawning()
    {
        int ticksPerSecond = 10; // Change here.
        while (true)
        {
            if (GameManager.Instance.isSpawnAllowed 
                && Time.realtimeSinceStartup - GameManager.Instance.lastDeathTime >= spawnDelay
                && currMonster == null)
                SpawnEnemies();
            // После победы над необходимым количеством врагов открывается следующая локация.
            if (Player.maxLocationOpened <= id && enemiesSpawned > 10)
            {
                Player.maxLocationOpened ++;
                MapButton.Instance.StartAnimation();
            }
            yield return new WaitForSeconds((float)1 / ticksPerSecond);
        }
    }

    // создаем нового монстра
    public void SpawnEnemies()
    {
        GameObject monsterForSpawn = ProportionalWheelSelection.SelectMonster (monstersPool);
        currMonster = Instantiate(monsterForSpawn, outerPoint.position, Quaternion.identity).GetComponent<Monster>();
        currMonster.isInvulnerable = true;

        // Random spawn from left or right screen side. Aslo turn it face to player (all raw sprites are left-faced).
        if (currMonster.type == Monster.Type.ground)
        {
            if (Random.Range(0, 2) == 0)
            {
                // spawn at left
                currMonster.gameObject.transform.position = leftGroundSpawn.position;
                currMonster.FaceToRight();
            }
            else
            {
                // spawn at right
                currMonster.gameObject.transform.position = rightGroundSpawn.position;
            }
            MoveEnemyToScreen(currMonster, centralGroundPoint.position);
        }
        else if (currMonster.type == Monster.Type.fly)
        {
            if (Random.Range(0, 2) == 0)
            {
                // spawn at left
                currMonster.gameObject.transform.position = leftFlySpawn.position;
                currMonster.FaceToRight();
            }
            else
            {
                // spawn at right
                currMonster.gameObject.transform.position = rightFlySpawn.position;
            }
            MoveEnemyToScreen (currMonster, centralFlyPoint.position);
        }
        enemiesSpawned ++;
    }

    void MoveEnemyToScreen (Monster enemy, Vector3 position)
    {
        LeanTween.move(enemy.gameObject, position, 0.25f).setOnComplete(() =>
        {
            enemy.isInvulnerable = false;
            MenuController.Instance.RefreshEnemyHealthBar();
        });
    }

    // Если у локации нет собственных особенных точек для спавна врагов, то она берет обычные.
    private void SetupPoints()
    {
        if (leftGroundSpawn == null)
            leftGroundSpawn = standartPoints.leftGroundSpawn;
        if (rightGroundSpawn == null)
            rightGroundSpawn = standartPoints.rightGroundSpawn;
        if (centralGroundPoint == null)
            centralGroundPoint = standartPoints.centralGroundPoint;
        if (leftFlySpawn == null)
            leftFlySpawn = standartPoints.leftFlySpawn;
        if (rightFlySpawn == null)
            rightFlySpawn = standartPoints.rightFlySpawn;
        if (centralFlyPoint == null)
            centralFlyPoint = standartPoints.centralFlyPoint;
        if (leftEscapeGroundPoint == null)
            leftEscapeGroundPoint = standartPoints.leftEscapeGroundPoint;
        if (rightEscapeGroundPoint == null)
            rightEscapeGroundPoint = standartPoints.rightEscapeGroundPoint;
        if (leftEscapeFlyPoint == null)
            leftEscapeFlyPoint = standartPoints.leftEscapeFlyPoint;
        if (rightEscapeFlyPoint == null)
            rightEscapeFlyPoint = standartPoints.rightEscapeFlyPoint;
        if (outerPoint == null)
            outerPoint = standartPoints.outerPoint;
    }
}
