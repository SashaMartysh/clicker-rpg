﻿using UnityEngine;

public class OptionsScreen : MonoBehaviour
{
    public GameObject quitButton;

    void OnEnable()
    {
        if (GameManager.Instance.currentState == GameManager.State.MainMenu)
            quitButton.SetActive(false);
        else quitButton.SetActive(true);
    }
}
