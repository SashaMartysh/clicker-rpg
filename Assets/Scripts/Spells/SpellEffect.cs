﻿using MarchingBytes;
using UnityEngine;

public class SpellEffect : MonoBehaviour
{
    public Spell spell; // Ссылка к заклинанию-родителю, к которому относится этот эффект.

    public AudioSource audioSource;
    public AudioClip[] audioClips;


    /// <summary>
    /// Play random sound.
    /// Проиграть случайный звуковой эффект.
    /// </summary>
    public void PlaySound()
    {
        audioSource.clip = audioClips[Random.Range(0, audioClips.Length)];
        audioSource.Play();
    }

    public void ReturnToPool()
    {
        EasyObjectPool.instance.ReturnObjectToPool (gameObject);
    }
}
