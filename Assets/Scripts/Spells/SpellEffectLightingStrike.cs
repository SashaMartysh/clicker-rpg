﻿using UnityEngine;

public class SpellEffectLightingStrike : SpellEffect
{


    void OnEnable()
    {
        //Разместить молнию по середине сверху экрана.
        Camera c = Camera.main;
        gameObject.transform.position = c.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height, 10f));

        int face = Random.Range(0, 2) == 0 ? -1 : 1;
        gameObject.transform.localScale = new Vector3((1f+spell.level/100f)*face, 1f + spell.level / 100f, 1f);

        if (Level.Instance.currMonster != null && Level.Instance.currMonster.isInvulnerable == false)
        {
            float damageDispresion = Random.Range(0.7f, 1.3f);
            Hit hit = new Hit(
                isSuccess: true,
                isCritical: false,
                damage: (spell.Damage() * damageDispresion) * (1f - Level.Instance.currMonster.lightingResist),
                type: Hit.Type.lighting);
            Level.Instance.currMonster.TakeHit(hit);
        }
        PlaySound();

        Invoke("ReturnToPool", 1.4f);
        //Destroy(gameObject, 0.75f);
    }
}
