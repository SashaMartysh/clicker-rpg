﻿using UnityEngine;

[CreateAssetMenu(menuName = "Spell")]
public class Spell : ScriptableObject
{
    public int id;
    public string name;
    public int level;
    public bool isNeedTargetForCast = true;
    public float castInterval;
    public Sprite sprite;
    public SpellEffect spellEffect;
    public SpellUpgraide[] upgraides;

    public FNumber Damage()
    {
        if(level > 0)
            return Upgraides()[level - 1].damage;
        else return new FNumber(0f);
    }

    public SpellUpgraide[] Upgraides()
    {
        return upgraides;
    }

    public float ManaCost()
    {
        return upgraides[level - 1].manaCost.ToFloat();
    }
}

[System.Serializable]
public struct SpellUpgraide
{
    public FNumber damage;
    public FNumber cost;
    public FNumber manaCost;
}

