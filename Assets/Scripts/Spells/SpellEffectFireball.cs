﻿using MarchingBytes;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpellEffectFireball : SpellEffect
{
    public ParticleSystem particleSystem;
    public GameObject ballImage;

    void OnEnable()
    {
        Debug.Log("Fireball OnEnable");
        Camera c = Camera.main;
        Vector3 startPosition = c.ScreenToWorldPoint(new Vector3(Screen.width * Random.Range (0f, 1f), Screen.height, 10f));
        Vector3 destinationPosition = GameManager.Instance.currLevel.centralGroundPoint.position;
        transform.position = startPosition;

        var dir = destinationPosition - transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        ballImage.SetActive(true);
        particleSystem.Clear();
        particleSystem.Stop(false);


        LeanTween.move(gameObject, GameManager.Instance.currLevel.centralGroundPoint.position, 1f).setOnComplete(Impact);
    }

    void Impact()
    {
        if (Level.Instance.currMonster != null && Level.Instance.currMonster.isInvulnerable == false)
        {
            float damageDispresion = Random.Range(0.9f, 1.1f);
            Hit hit = new Hit(
                isSuccess: true,
                isCritical: false,
                damage: (spell.Damage() * damageDispresion) * (1f - Level.Instance.currMonster.fireResist),
                type: Hit.Type.fire);
            Level.Instance.currMonster.TakeHit(hit);
        }

        ballImage.SetActive(false);
        particleSystem.Play();

        PlaySound();
        Invoke("ReturnToPool", 1f);
        //EasyObjectPool.instance.ReturnObjectToPool(gameObject);
        //Destroy(gameObject, 0.35f);
    }

}
