﻿using UnityEngine;

public class SpellEffectIceShards : SpellEffect
{
    // Called by animation.
    public void StartRising()
    {
        PlaySound();
    }

    // Called by animation.
    public void Impact()
    {
        if (Level.Instance.currMonster != null && Level.Instance.currMonster.isInvulnerable == false)
        {
            float damageDispresion = Random.Range(0.9f, 1.1f);
            Hit hit = new Hit(
                isSuccess: true,
                isCritical: false,
                damage: (spell.Damage() * damageDispresion) * (1f - Level.Instance.currMonster.coldResist),
                type: Hit.Type.cold);
            Level.Instance.currMonster.TakeHit(hit);
        }
    }

    // Called by animation.
    public void DestroyEffect()
    {
        Invoke("ReturnToPool", 0f);
    }
}
