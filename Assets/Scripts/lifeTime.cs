﻿using UnityEngine;

public class LifeTime : MonoBehaviour
{

    public float lifeTime;   // время, через которое объект будет автоматически уничтожен

	// Use this for initialization
	void Start () {
        Destroy(gameObject, lifeTime); 
	}

}
