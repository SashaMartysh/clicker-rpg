﻿using UnityEngine;

public class SprayedParticle : MonoBehaviour
{
    public float pushForce;

	void Start () {
        ImitateParticle();
	}

    public void ImitateParticle()
    {
        Vector2 direction = new Vector2(UnityEngine.Random.Range(-1f, 1f),
                                  UnityEngine.Random.Range(0.5f, 1f));
        // GetComponent<Rigidbody2D>().velocity = direction;
        GetComponent<Rigidbody2D>().AddForce(direction * pushForce);
    }
}
