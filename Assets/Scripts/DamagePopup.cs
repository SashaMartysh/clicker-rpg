﻿using MarchingBytes;
using UnityEngine;
using TMPro;
using SmartLocalization;

public class DamagePopup : MonoBehaviour
{
    public TextMeshPro labelValue;
    public GameObject powImage;
    public RectTransform rectTransform;

    [Header("Settings")]
    public float standartScale = 0.8f;
    public float criticalHitScale = 1.2f;

    public void SetValue(Hit hit)
    {
        if (hit.isSuccess)
        {
            labelValue.text = hit.dmg.ToString();
            labelValue.fontStyle = FontStyles.Normal;
            if (hit.isCritical)
            {
                powImage.SetActive(true);
                gameObject.transform.localScale = new Vector3(criticalHitScale, criticalHitScale);
                Time.timeScale = 0.5f;
                GameManager.Instance.isSlowmoAfterCriticalHit = true;
            }
            else
            {
                powImage.SetActive(false);
                gameObject.transform.localScale = new Vector3(standartScale, standartScale);
            }
            // Set Color of popup.
            switch (hit.type)
            {
                case Hit.Type.melee:
                    {
                        labelValue.color = MyColors.LightYellow();
                        break;
                    }
                case Hit.Type.lighting:
                    {
                        labelValue.color = Color.white;
                        break;
                    }
                case Hit.Type.cold:
                    {
                        labelValue.color = MyColors.ColdElement();
                        break;
                    }
                case Hit.Type.fire:
                {
                    labelValue.color = MyColors.FireElement();
                        break;
                    }
            }

        }
        else if (!hit.isSuccess)
        {
            powImage.SetActive(false);
            labelValue.text = LanguageManager.Instance.GetTextValue("L_miss");
            labelValue.color =Color.gray;
            labelValue.fontStyle = FontStyles.Italic;
        }

        rectTransform.rotation = Quaternion.Euler(0f, 0f , Random.Range(-30f, 30f));
    }

    public static void PopupGamage (Hit hit)
    {
        // местоположение попапа на экране
        Vector3 position = new Vector3((float)UnityEngine.Random.Range(-200, 200) / 100,
            (float)UnityEngine.Random.Range(0, 300) / 100,
            0);
        GameObject popupDamage = EasyObjectPool.instance.GetObjectFromPool("damagePopupsPool", position,
            Quaternion.identity);
        popupDamage.GetComponent<DamagePopup>().SetValue(hit);
    }
}
