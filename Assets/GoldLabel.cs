﻿using UnityEngine;
using TMPro;

public class GoldLabel : MonoBehaviour
{
    public TextMeshProUGUI label;

    void Update()
    {
        label.text = Player.gold.ToString() + "<sprite=0>";
    }
}
