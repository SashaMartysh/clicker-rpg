﻿using UnityEngine;
using TMPro;
using SmartLocalization;

public class InGamePurchasesPanel : MonoBehaviour
{

    public TextMeshProUGUI labelConsumable1;
    public TextMeshProUGUI labelConsumable2;

    void OnEnable()
    {
        labelConsumable1.text = LanguageManager.Instance.GetTextValue("L_PileOfGold") + " " +
                                Player.Instance.CurrentConsumable1().ToString() + "<sprite=0>\n"
                                + LanguageManager.Instance.GetTextValue("L_for") + " " + "0.99$";
        labelConsumable2.text = LanguageManager.Instance.GetTextValue("L_BigPileOfGold") + " " +
                        Player.Instance.CurrentConsumable2().ToString() + "<sprite=0>\n"
                        + LanguageManager.Instance.GetTextValue("L_for") + " " + "2.49$";
    }
}
