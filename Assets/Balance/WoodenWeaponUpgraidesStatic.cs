﻿using UnityEngine;

namespace StaticData
{
    public class WoodenWeaponUpgraidesStatic : ScriptableObject
    {
        public WeaponUpgraideData[] woodenWeaponUpgraidesList;
    }

    [System.Serializable]
    public struct WeaponUpgraideData
    {
        public int dmg;
        public int cost;
    }
}

