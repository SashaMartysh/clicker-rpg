﻿using UnityEngine;

public class ColorScheme : MonoBehaviour
{
    public static ColorScheme Instance;

    public Color CurrentColor = Color.red;

    public delegate void ColorSchemeChangeDelegate (Color color);
    public event ColorSchemeChangeDelegate onColorSchemeChange;

    // Use this for initialization
    void Awake ()
	{
	    Instance = this;
	}

    public void ChangeColorScheme()
    {
        if (onColorSchemeChange != null)
            onColorSchemeChange (CurrentColor);
    }
}
